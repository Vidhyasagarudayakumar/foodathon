from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from crops.models import Crop


class Create_crop(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(Create_crop, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Submit"))

    class Meta:
        model = Crop
        fields = ['crop_type', 'crop_name', 'seed_info', 'seed_info_image', 'soil_nutrition_info',
                  'soil_nutrition_info_image', 'protection_1',
                  'protection_1_image', 'protection_2', 'protection_2_image', 'protection_3', 'protection_3_image',
                  'protection_4', 'protection_4_image',
                  'yield_estimation_info', 'yield_estimation_image', 'schemes_info', 'schemes_image', 'insurance_info',
                  'insurance_image']

