# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.shortcuts import render, redirect
# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from crops.forms import Create_crop
from crops.models import Crop
from crops.serializers import CropSerializer


def enter_new_crop(request):
    forms = Create_crop()
    if request.method == "POST":
        forms = Create_crop(request.POST,request.FILES)
        if forms.is_valid():
            crop = forms.save()
            messages.success(request, "Added!!")
            return redirect('enter_new_crop')
    return render(request, 'crops/create_new_crop.html', {'forms': forms})


class Crop_data(APIView):
    serializer_class = CropSerializer

    def get(self, request):
        cropList = []
        crops = Crop.objects.all()
        for id, i in enumerate(crops, start=1):
            cropList.append(
                {"id": id, "name": i.crop_name, "crop_type": i.get_crop_type_display(), "seed_info": i.seed_info,
                 "insurance_info": i.insurance_info, "protection_info_1": i.protection_1,
                 'schemes_info': i.schemes_info, 'yield_estimation_info': i.yield_estimation_info,
                 'soil_nutrition_info': i.soil_nutrition_info,'protection_1_image': i.protection_1_image.url,
                 'protect_2_image': i.protection_2_image.url, 'protect_2': i.protection_2, 'protect_3': i.protection_3,
                 'protect_3_image': i.protection_3_image.url, 'protect_4': i.protection_4,
                 'protect_4_image': i.protection_4_image.url, 'seed_info_image': i.seed_info_image.url,
                 'insurance_image': i.insurance_image.url, 'schemes_image': i.schemes_image.url,
                 'yield_estimation_image': i.yield_estimation_image.url,'soil_nutrition_image':i.soil_nutrition_info_image.url})
        return Response(cropList)



def sample(request):
    crop_image = Crop.objects.all()[0].protection_1_image.url
    return render(request,'crops/sample.html',{'image':crop_image})