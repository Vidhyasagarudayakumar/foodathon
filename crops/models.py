# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Crop(models.Model):
    CROP_CHOICES = ((-1,'None'),(0,'Karief'),(1,'Rabi'),(2,'Zaid'),(3,'Plantation'))
    crop_type = models.IntegerField(choices=CROP_CHOICES,default=-1)
    crop_name = models.CharField(null=True,blank=True,max_length=255)
    seed_info = models.TextField(null=True,blank=True,max_length=100000)
    seed_info_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    soil_nutrition_info = models.TextField(null=True,blank=True,max_length=100000)
    soil_nutrition_info_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    protection_1 = models.TextField(null=True,blank=True,max_length=100000)
    protection_2 = models.TextField(null=True,blank=True,max_length=100000)
    protection_3 = models.TextField(null=True,blank=True,max_length=100000)
    protection_4 = models.TextField(null=True,blank=True,max_length=100000)
    yield_estimation_info = models.TextField(null=True,blank=True,max_length=100000)
    yield_estimation_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    schemes_info = models.TextField(null=True,blank=True,max_length=100000)
    schemes_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    insurance_info = models.TextField(null=True,blank=True,max_length=100000)
    insurance_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    protection_1_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    protection_2_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    protection_3_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)
    protection_4_image = models.ImageField(default='images/crossed.png',upload_to='images/',null=True,blank=True)


