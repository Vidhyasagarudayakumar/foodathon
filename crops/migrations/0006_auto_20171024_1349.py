# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-24 13:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crops', '0005_auto_20171024_1327'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='crop',
            name='h_crop_name',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='h_insurance_info',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='h_protection_info',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='h_schemes_info',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='h_seed_info',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='h_soil_nutrition_info',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='h_yield_estimation_info',
        ),
    ]
