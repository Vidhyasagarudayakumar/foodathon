# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-23 22:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crops', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='crop',
            name='h_crop_name',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crop',
            name='h_crop_type',
            field=models.IntegerField(choices=[(-1, 'None'), (0, 'Karief'), (1, 'Rabi'), (2, 'Zaid'), (3, 'Plantation')], default=-1),
        ),
        migrations.AddField(
            model_name='crop',
            name='h_insurance_info',
            field=models.TextField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crop',
            name='h_protection_info',
            field=models.TextField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crop',
            name='h_schemes_info',
            field=models.TextField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crop',
            name='h_seed_info',
            field=models.TextField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crop',
            name='h_soil_nutrition_info',
            field=models.TextField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crop',
            name='h_yield_estimation_info',
            field=models.TextField(default=1, max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='crop',
            name='crop_type',
            field=models.IntegerField(choices=[(-1, 'None'), (0, 'Karief'), (1, 'Rabi'), (2, 'Zaid'), (3, 'Plantation')], default=-1),
        ),
    ]
